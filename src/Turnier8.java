
import javax.swing.*;

//Program to simulate a soccer competition with 8 teams 

/**
 * 
 * @author KMB<kili.bender@t-online.de>
 *
 */

/*
 * 
 * !THIS IS STILL A BETAVERSION! 
 * 
 * SEND BUGS TO KMB WITH STACKTRACE FOR UNHANDLED ERRORS OR DESCRIPTION OF THE MISBEHAVIOUR
 * PARTS OF THIS PROGRAMM ARE IN GERMAN BECAUSE IT WAS WRITTEN FOR GERMAN USERS
 * 
 */


//main class to start up the process

public class Turnier8 {
	
	public static void main(String args[]){
		Team[] teams = new Team[8];
		int semi = JOptionPane.showConfirmDialog(null, "Mit Halbfinale?");
		if(semi == JOptionPane.YES_OPTION)
			semi = 1;
		else
			semi = 0;
		
		for(int i = 0; i < 4; i++){
			//this is the correct line
			teams[i] =  new Team(JOptionPane.showInputDialog("Geben Sie die Mannschaften der Gruppe A einzeln ein:"), 0, 0, 0, 0, i);
			char a = 'a';//testing
			//teams[i] = new Team(String.valueOf((char)(a + i)), 0, 0, 0, 0 , i);//testing
		}
		for(int i = 4; i < 8; i++){
			//this is the correct line
			//teams[i] = new Team(JOptionPane.showInputDialog("Geben Sie die Mannschaften der Gruppe B einzeln ein:"), 0, 0, 0, 0, i);
			
			char a = 'a';//testing
			teams[i] = new Team(String.valueOf((char)(a + i)), 0, 0, 0, 0 , i);//testing
		}
		
		new MainFrame(teams , semi);
	}
}
