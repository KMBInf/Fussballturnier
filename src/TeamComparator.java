
import java.util.Comparator;

public class TeamComparator implements Comparator<Team>{
	private int[][] dv;
	public TeamComparator(int[][] dv){
		this.dv = dv;
	}
	@Override public int compare(Team t1, Team t2){
		if(t1.getValue() != t2.getValue()){
			return Integer.compare( t2.getValue(), t1.getValue());
		}else if(t1.getPoints()!= t2.getPoints()){
			return Integer.compare( t2.getPoints(), t1.getPoints());
		}else if(dv[t1.getNummer()][t2.getNummer()] != 0){
			return dv[t1.getNummer()][t2.getNummer()];
		}else if(t1.getGoals() - t1.getMinusgoals() != t2.getGoals() - t2.getMinusgoals()) {
			return Integer.compare( t2.getGoals() - t2.getMinusgoals(), t1.getGoals() - t1.getMinusgoals());
		}else{
			return Integer.compare( t2.getGoals() , t1. getGoals());
		}
	}
}